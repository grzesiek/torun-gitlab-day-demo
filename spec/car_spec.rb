require_relative '../car'

describe Car do
  describe '#ignite!' do
    it 'starts an engine' do
      expect(subject.ignite!).to eq 'engine on'
    end
  end

  describe '#color' do
    it 'is a red car' do
      expect(subject.color).to eq 'white'
    end
  end
end
